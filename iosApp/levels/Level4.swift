//
//  Level4.swift
//  iosApp
//
//  Created by user153865 on 4/22/19.
//  Copyright © 2019 stc010. All rights reserved.
//
import SpriteKit
import AVFoundation

class Level4 : LevelScene {

    required init(size: CGSize, level: Int) {
        let switch1 = Switch(gridColumn: 1)
        let switch2 = Switch(gridColumn: 2)
        let switch3 = Switch(gridColumn: 3)
        let switch4 = Switch(gridColumn: 4)
        let switch5 = Switch(gridColumn: 5)
        let switch6 = Switch(gridColumn: 6)
        let switch7 = Switch(gridColumn: 7)
        let switch8 = Switch(gridColumn: 8)
        
        let gate1 = OrGate(gridRow: 5, gridColumn: 2, inputA: switch2, inputB: switch3)
        let gate2 = AndGate(gridRow: 5, gridColumn: 3, inputA: switch4, inputB: switch5)
        let gate3 = AndGate(gridRow: 5, gridColumn: 4, inputA: switch6, inputB: switch7)
        let gate4 = OrGate(gridRow: 3, gridColumn: 1, inputA: switch1, inputB: gate1)
        let gate5 = XorGate(gridRow: 3, gridColumn: 2, inputA: gate1, inputB: gate2)
        let gate6 = AndGate(gridRow: 3, gridColumn: 4, inputA: gate2, inputB: gate3)
        let gate7 = XorGate(gridRow: 3, gridColumn: 5, inputA: gate3, inputB: switch8)
        let gate8 = AndGate(gridRow: 2, gridColumn: 2, inputA: gate4, inputB: gate5)
        let gate9 = AndGate(gridRow: 2, gridColumn: 4, inputA: gate6, inputB: gate7)
        let gate10 = OrGate(gridRow: 1, gridColumn: 3, inputA: gate8, inputB: gate9)
        
        let cpu = Cpu(input: gate10)
        
        super.init(size : size, cpu: cpu, level: level)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
