//
//  Level1.swift
//  iosApp
//
//  Created by user153865 on 4/22/19.
//  Copyright © 2019 stc010. All rights reserved.
//
import SpriteKit
import AVFoundation

class Level1 : LevelScene {
    
    required init(size: CGSize, level: Int) {
        let switch1 = Switch(gridColumn: 1)
        let switch2 = Switch(gridColumn: 2)
        let switch3 = Switch(gridColumn: 3)
    
        let gate1 = NotGate(input: switch1, gridRow: 4, gridColumn: 2)
        let gate2 = OrGate(gridRow: 4, gridColumn: 4, inputA: switch2, inputB: switch3)
        let gate3 = AndGate(gridRow: 2, gridColumn: 3, inputA: gate1, inputB: gate2)
    
        let cpu = Cpu(input: gate3)
    
        super.init(size: size, cpu: cpu, level: level)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
        
    /* Example of dynamically generated imageView
        let image = UIImage(named: "images/" + gate1.getGatePicture())
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: 150, y: 250, width: 50, height: 50)
        view.addSubview(imageView)
    */
    
}

