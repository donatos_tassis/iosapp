//
//  Level2.swift
//  iosApp
//
//  Created by user153865 on 4/22/19.
//  Copyright © 2019 stc010. All rights reserved.
//
import SpriteKit
import AVFoundation

class Level2 : LevelScene {

    required init(size: CGSize, level: Int) {
        let switch1 = Switch(gridColumn: 1)
        let switch2 = Switch(gridColumn: 2)
        let switch3 = Switch(gridColumn: 3)
        let switch4 = Switch(gridColumn: 4)
        
        let gate1 = AndGate(gridRow: 4, gridColumn: 4, inputA: switch3, inputB: switch4)
        let gate2 = OrGate(gridRow: 4, gridColumn: 2, inputA: switch1, inputB: switch2)
        let gate3 = OrGate(gridRow: 2, gridColumn: 3, inputA: gate2, inputB: gate1)
        
        let cpu = Cpu(input: gate3)
        
        super.init(size : size, cpu: cpu, level: level)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
