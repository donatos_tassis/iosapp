//
//  Level3.swift
//  iosApp
//
//  Created by user153865 on 4/22/19.
//  Copyright © 2019 stc010. All rights reserved.
//
import SpriteKit
import AVFoundation

class Level3 : LevelScene {

    required init(size: CGSize, level: Int) {
        let switch1 = Switch(gridColumn: 1)
        let switch2 = Switch(gridColumn: 2)
        let switch3 = Switch(gridColumn: 3)
        let switch4 = Switch(gridColumn: 4)
        let switch5 = Switch(gridColumn: 5)
        let switch6 = Switch(gridColumn: 6)

        let gate1 = AndGate(gridRow: 5, gridColumn: 1, inputA: switch1, inputB: switch2)
        let gate2 = XorGate(gridRow: 5, gridColumn: 3, inputA: switch3, inputB: switch4)
        let gate3 = AndGate(gridRow: 5, gridColumn: 5, inputA: switch5, inputB: switch6)
        let gate4 = NotGate(input: gate2, gridRow: 4, gridColumn: 3)
        let gate5 = OrGate(gridRow: 3, gridColumn: 2, inputA: gate1, inputB: gate2)
        let gate6 = OrGate(gridRow: 3, gridColumn: 4, inputA: gate4, inputB: gate3)
        let gate7 = XorGate(gridRow: 2, gridColumn: 3, inputA: gate5, inputB: gate6)
        let gate8 =  NotGate(input: gate7, gridRow: 1, gridColumn: 3)
        
        let cpu = Cpu(input: gate8)
        
        super.init(size : size, cpu: cpu, level: level)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
     
}
