//
//  LevelViewController.swift
//  iosApp
//
//  Created by user153865 on 4/19/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import SpriteKit
import UIKit

class LevelViewController : UIViewController {
    
    var levelNumber: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load the selected level initializer
        let aClass = stringClassFromString("Level" + String(self.levelNumber)) as! LevelScene.Type
        let scene = aClass.init(size: view.bounds.size, level: levelNumber)
                
        let skView = view as! SKView
        scene.scaleMode = .aspectFill
        skView.presentScene(scene)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnSwipe = false
    }
    
    // get class by string name
    func stringClassFromString(_ className: String) -> AnyClass! {
        
        /// get namespace
        let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;
        
        /// get 'anyClass' with classname and namespace
        let cls: AnyClass = NSClassFromString("\(namespace).\(className)")!;
        
        // return AnyClass!
        return cls;
    }
}
