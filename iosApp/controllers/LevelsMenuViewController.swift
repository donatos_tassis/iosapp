//
//  LevelsViewController.swift
//  iosApp
//
//  Created by user153865 on 4/19/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import UIKit

class LevelsMenuViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, LevelDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var levelsCollection: UICollectionView!
    
    // ToDo: need to add the levels dynamically
    let levels:[String] = ["1", "2", "3", "4"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnSwipe = false
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionView.reloadData()
        collectionView.backgroundColor = UIColor .clear
        
        return levels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "buttonCell", for: indexPath) as! Level
        
        let btnImg = UIImage(named: "level_button.jpg") as UIImage?
        let btnImgPassed = UIImage(named: "led_green.jpg") as UIImage?
        let selectedBtnImg = UIImage(named: "led_blue.png") as UIImage?
        
        cell.button.setBackgroundImage(btnImg, for: .normal)
        
        let levelsPassed = getLevelsPassed()
        let lvlsPassed = levelsPassed.components(separatedBy: ",")
        for lvl in lvlsPassed {
            if(lvl == levels[indexPath.row]) {
                cell.button.setBackgroundImage(btnImgPassed, for: .normal)
                break;
            }
        }
        cell.button.setTitle(levels[indexPath.row], for: .normal)
        cell.button.titleLabel!.font = UIFont.boldSystemFont(ofSize: 20)
        cell.button.setTitleColor(.white, for: .normal)
        cell.button.setBackgroundImage(selectedBtnImg, for: .selected)
        cell.button.tag = Int(levels[indexPath.row])!
        cell.delegate = self
        
        return cell
    }
    
    func didTapLevelStart(button: UIButton) {
//        self.performSegue(withIdentifier: "startLevelSegue", sender: button)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  "startLevelSegue" {
            let destinationVC = segue.destination as! LevelViewController
            if let button = sender as? UIButton {
                destinationVC.levelNumber = button.tag
            }
        }
    }

    func getLevelsPassed() -> String {
        let fileName = "levels"
        let levelsFile = Bundle.main.url(forResource: fileName, withExtension: "txt")
        var levelsData:String = ""
        do {
            levelsData = try String(contentsOf: levelsFile!, encoding: .utf8)
        } catch let error as NSError {
            print("failed to read file")
            print(error)
        }
        return levelsData
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController == self {
            self.levelsCollection.reloadData()
        }
    }
}
