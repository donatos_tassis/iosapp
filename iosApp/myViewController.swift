//
//  myViewController.swift
//  iosApp
//
//  Created by user153865 on 4/23/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation
import UIKit

class myViewController : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.hidesBarsOnTap = false
    }
}
