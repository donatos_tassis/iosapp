//
//  Level.swift
//  iosApp
//
//  Created by user153865 on 4/19/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import UIKit

protocol LevelDelegate {
    func didTapLevelStart(button: UIButton)
}

class Level: UICollectionViewCell {
        
    @IBOutlet weak var button: UIButton!
    
    var delegate: LevelDelegate!
    
    @IBAction func levelStart(_ sender: UIButton) {
        self.delegate?.didTapLevelStart(button: button)
    }
}
