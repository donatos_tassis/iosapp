//
//  LevelScene.swift
//  iosApp
//
//  Created by user153865 on 4/19/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import SpriteKit
import AVFoundation

class LevelScene : SKScene {
    
    // number of level
    var level : Int?
    // indicates if the given level is passed or not
    var lvlComplete : Bool = false
    // the Cpu instance of the level
    var cpu : Cpu?
    // array of component that already seen (used to build the components recursivilly)
    var componentSeen : [Component] = []
    // dictionary of components with their corresponding tag
    var components : [String : Component] = [:]
    // dictionary of components tags with their corresponding coordinates(to draw lines)
    var componentsCoord : [String : [CGFloat]] = [:]
    // array of Switch components
    var switches : [Switch] = []
    // the stack view the holds the switches
    var switchesView : SwitchStackView! = SwitchStackView()
    // the total width of the switches stack view
    var switchWidth : CGFloat!
    
    // screen size
    var screenRect : CGRect! = UIScreen.main.bounds
    var screenWidth : CGFloat! = UIScreen.main.bounds.size.width
    var screenHeight : CGFloat! = UIScreen.main.bounds.size.height
    
    // the image of the cpu, initially set to inactive
    var cpuImage = SKSpriteNode(imageNamed: "cpu_inactive.jpg")
    
    // images for switches (active/inactive)
    let imageInactive = UIImage(named: "led_red.jpg")
    let imageActive = UIImage(named: "led_green.jpg")
    // cpu images
    let cpuInactive = "cpu_inactive.jpg"
    let cpuActive = "cpu_active.jpg"
    // grid attributes
    let gridRows = 5
    let gridColumns = 5
    
    // sounds
    let soundButton = SKAction.playSoundFileNamed("push_button.wav", waitForCompletion: false)
    let soundVictory = SKAction.playSoundFileNamed("victory.wav", waitForCompletion: false)
    
    required init(size: CGSize, level: Int) {
        self.level = level
        super.init(size: size)
    }
    
    init(size: CGSize, cpu: Cpu, level: Int)
    {
        self.cpu = cpu
        self.level = level
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didMove(to view: SKView) {
        setComponents(component : cpu!.input)
        components[cpu!.imageViewTag] = cpu
        layoutScene()
        drawWires()
    }
    
    func layoutScene() {
        // background image
        let backgroundImage  = SKSpriteNode(imageNamed: "circuit_bg.jpg")
        backgroundImage.position = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
        backgroundImage.size = CGSize(width: frame.size.width, height: frame.size.height)
        addChild(backgroundImage)
        
        // cpu image
        cpuImage.size = CGSize(width: frame.size.width/3, height: frame.size.width/3)
        cpuImage.position = CGPoint(x: frame.midX, y: frame.maxY - cpuImage.size.height/2)
        addChild(cpuImage)
        
        //get cpu coordinates
        let cpuRect = cpuImage.calculateAccumulatedFrame()
        componentsCoord[cpu!.getImageViewTag()] = [cpuRect.midX, cpuRect.minY + (0.12 * cpuRect.height)]
        
        // switches stack view (bottom of screen)
        switchWidth = switchesWidth(screenWidth: screenWidth)
        switchesView.scene = self
        switchesView.frame.size = CGSize(width: switchWidth, height: screenHeight/15)
        setSwitchesLayout(stackView: switchesView)
        switchesView.axis = .horizontal
        switchesView.spacing = 2.0
        switchesView.alignment = .center
        switchesView.distribution = .fillEqually
        switchesView.accessibilityFrame = switchesView.frame
        view?.addSubview(switchesView)
        
        // switches stackView Constraints
        switchesView.translatesAutoresizingMaskIntoConstraints = false
        switchesView.widthAnchor.constraint(equalToConstant: switchWidth).isActive = true
        switchesView.centerXAnchor.constraint(equalTo: view!.centerXAnchor).isActive = true
        switchesView.heightAnchor.constraint(equalToConstant: screenHeight/15).isActive = true
        switchesView.bottomAnchor.constraint(equalTo: view!.bottomAnchor, constant: -25).isActive = true
        
        // gates grid setup
        let gridHeight = frame.height - frame.size.width/3 * 1.2 - screenHeight/15
        let blockWidth = frame.width / CGFloat(gridColumns)
        let blockHeight = gridHeight / CGFloat(gridRows)
        
        if let grid = Grid(blockWidth: blockWidth, blockHeight: blockHeight, rows: gridRows, columns: gridColumns) {
            grid.size = CGSize(width: screenWidth, height: gridHeight)
            grid.position = CGPoint(x:frame.midX, y:frame.maxY - cpuImage.size.height - gridHeight/2)
            addChild(grid)
            
            for (_, component) in components {
                if(component.getType() == "Gate" || component.getType() == "NotGate") {
                    let gate = SKSpriteNode(imageNamed: component.getGatePicture())
                    if(blockWidth < 100) {
                        gate.setScale(0.65)
                    }
                    gate.position = grid.gridPosition(row: component.getGridRow()-1, column: component.getGridColumn()-1)
                    grid.addChild(gate)
                    
                    // get gates coordinates
                    let gRect = gate.calculateAccumulatedFrame()
                    let gatePos = convert(gate.position, from: grid)
                    
                    componentsCoord[component.getImageViewTag()] = [
                        gatePos.x,
                        gatePos.y + (gRect.maxY - gRect.midY),
                        gatePos.x,
                        gatePos.y - (gRect.midY - gRect.minY)
                    ]
                }
            }
            
        }
    }
    
    // recursive function that populates the components list and the switches list
    func setComponents(component : Component) {
        if(!componentSeen.contains {$0 === component}) {
            componentSeen.append(component)
            if(component.getType() == "Gate") {
                let comp = component as! Gate
                components[comp.imageViewTag!] = comp
                setComponents(component: comp.inputA!)
                setComponents(component: comp.inputB!)
            } else if(component.getType() == "NotGate") {
                let comp = component as! NotGate
                components[comp.imageViewTag!] = comp
                setComponents(component: comp.input!)
            } else if(component.getType() == "Switch") {
                let comp = component as! Switch
                components[comp.imageViewTag] = comp
                switches.append(comp)
            }
        }
    }

    
    // sets the switches layout
    func setSwitchesLayout(stackView: SwitchStackView) {
        for switchObj in switches {
            let button = UIButton()
            button.accessibilityIdentifier = switchObj.getImageViewTag()
            button.tag = switchObj.getGridColumn()
            
            if(!switchObj.output()) {
                button.setBackgroundImage(imageInactive, for: .normal)
            } else {
                button.setBackgroundImage(imageActive, for: .normal)
            }
            
            stackView.addArrangedSubview(button)
            button.addTarget(self, action: #selector(onSwitchPress(button:)), for: .touchUpInside)
        }
    }

    
    // switch press listener
    @objc func onSwitchPress(button: UIButton!) {
        var switchObj : Switch?
        for switchComp in switches {
            if(switchComp.getGridColumn() == button.tag) {
                switchObj = switchComp
                break;
            }
        }
        run(soundButton)
        changeSwitchColorAndState(button: button, switchObj: switchObj)
        drawWires()
    }
    
    // calculate the total width of the switches UIStack element (bottom of screen)
    func switchesWidth(screenWidth: CGFloat) -> CGFloat {
        var switchWidth :CGFloat = 0
        switch switches.count {
        case 4:
            switchWidth = screenWidth * 0.85
        case 3:
            switchWidth = screenWidth * 0.65
        case 2:
            switchWidth = screenWidth * 0.5
        default:
            switchWidth = screenWidth
        }
        
        return switchWidth
    }
    
    // changes the backgroung image of the switch and its status on any press
    func changeSwitchColorAndState(button : UIButton, switchObj : Switch!) {
        switchObj.chanceStatus()
        if(switchObj.getStatus()) {
            button.setBackgroundImage(imageActive, for: .normal)
        } else {
            button.setBackgroundImage(imageInactive, for: .normal)
        }
        
        if(cpu!.output()) {
            if(!lvlComplete) {
                appendToFile()
            }

            cpuImage.texture = SKTexture(imageNamed: cpuActive)
            run(soundVictory)
            lvlComplete = true
            // ToDo: need to activate sensor , write into file for level passed , terminate level with a message
        } else {
            cpuImage.texture = SKTexture(imageNamed: cpuInactive)
        }
    }
    
    func appendToFile() {
        let delimeter = ","
        let fileName = "levels"
        let levelsFile = Bundle.main.url(forResource: fileName, withExtension: "txt")
        
        do {
            let levelsData = try String(contentsOf: levelsFile!, encoding: .utf8)
            let levelsPassed = levelsData.components(separatedBy: delimeter)
            var alreadyPassed = false
            for lvl in levelsPassed {
                if lvl == String(level!) {
                    alreadyPassed = true
                    break;
                }
            }
            if(!alreadyPassed) {
                let text = levelsData + delimeter + String(level!)
                try text.write(to: levelsFile!, atomically: true, encoding: .utf8)
            }
        } catch let error as NSError {
            print("Failed to write in file")
            print(error)
        }
    }
    
    func drawWires() {
        switchesView.layoutSubviews()
        for (tag, compCoords) in componentsCoord {
            if let component = components as? [String? : Component] {
                let component = component[tag] as! Component
                
                if(component.getType() == "Cpu") {
                    let cpu = component as! Cpu
                    let input = cpu.input
                    if let coords = componentsCoord as? [String? : [CGFloat]] {
                        let coords = coords[input.getImageViewTag()] as! [CGFloat]
                        drawLine(
                            from: CGPoint(x: coords[0], y: coords[1]),
                            to: CGPoint(x: compCoords[0], y: compCoords[1]),
                            status: input.output())
                    }
                }
                
                if(component.getType() == "NotGate") {
                    let notGate = component as! NotGate
                    let input = notGate.input
                    if let coords = componentsCoord as? [String? : [CGFloat]] {
                        let coords = coords[input!.getImageViewTag()] as! [CGFloat]
                        drawLine(
                            from: CGPoint(x: coords[0], y: coords[1]),
                            to: CGPoint(x: compCoords[2], y: compCoords[3]),
                            status: input!.output())
                    }

                }
                
                if(component.getType() == "Gate") {
                    let gate = component as! Gate
                    let inputA = gate.inputA
                    if let coordsA = componentsCoord as? [String? : [CGFloat]] {
                        let coordsA = coordsA[inputA!.getImageViewTag()] as! [CGFloat]
                        drawLine(
                            from: CGPoint(x: coordsA[0], y: coordsA[1]),
                            to: CGPoint(x: compCoords[2], y: compCoords[3]),
                            status: inputA!.output())
                    }
                    
                    let inputB = gate.inputB
                    if let coordsB = componentsCoord as? [String? : [CGFloat]] {
                        let coordsB = coordsB[inputB!.getImageViewTag()] as! [CGFloat]
                        drawLine(
                            from: CGPoint(x: coordsB[0], y: coordsB[1]),
                            to: CGPoint(x: compCoords[2], y: compCoords[3]),
                            status: inputB!.output())
                        
                    }
                }
            }
        }
    }
    
    // draw a line between two points
    func drawLine(from: CGPoint, to: CGPoint, status: Bool){
        var line = SKShapeNode()
        line.removeFromParent()
        let path = CGMutablePath()
        path.move(to: from)
        path.addLine(to: to)
        line = SKShapeNode(path: path)
        let lineWidth = screenWidth / CGFloat(gridColumns) * 0.05
        line.lineWidth = lineWidth
        line.strokeColor = .white
        
        if(status) {
            line.strokeColor = .blue
        }
        
        addChild(line)
    }
}
