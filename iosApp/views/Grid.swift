//
//  Grid.swift
//  iosApp
//
//  Created by user153865 on 4/24/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation
import SpriteKit

class Grid : SKSpriteNode {
    
    var rows: Int!
    var columns: Int!
    var blockWidth: CGFloat!
    var blockHeight: CGFloat!
    
    convenience init?(blockWidth: CGFloat, blockHeight: CGFloat, rows: Int, columns: Int) {
        guard let texture = Grid.gridTexture(blockWidth: blockWidth, blockHeight: blockHeight, rows: rows, columns:columns) else {
            return nil
        }
        self.init(texture: texture, color:SKColor.clear, size: texture.size())
        self.blockWidth = blockWidth
        self.blockHeight = blockHeight
        self.rows = rows
        self.columns = columns
    }
    
    class func gridTexture(blockWidth: CGFloat, blockHeight: CGFloat, rows:Int, columns:Int) -> SKTexture? {
        // Add 1 to the height and width to ensure the borders are within the sprite
        let size = CGSize(width: CGFloat(columns)*blockWidth+1.0, height: CGFloat(rows)*blockHeight+1.0)
        UIGraphicsBeginImageContext(size)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        let bezierPath = UIBezierPath()
        let offset:CGFloat = 0.5
        // Draw vertical lines
        for i in 0...columns {
            let x = CGFloat(i)*blockWidth + offset
            bezierPath.move(to: CGPoint(x: x, y: 0))
            bezierPath.addLine(to: CGPoint(x: x, y: size.height))
        }
        // Draw horizontal lines
        for i in 0...rows {
            let y = CGFloat(i)*blockHeight + offset
            bezierPath.move(to: CGPoint(x: 0, y: y))
            bezierPath.addLine(to: CGPoint(x: size.width, y: y))
        }
        SKColor.clear.setStroke()
        bezierPath.lineWidth = 1.0
        bezierPath.stroke()
        context.addPath(bezierPath.cgPath)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return SKTexture(image: image!)
    }
    
    func gridPosition(row:Int, column:Int) -> CGPoint {
        let offsetWidth = blockWidth / 2.0 + 0.5
        let x = CGFloat(column) * blockWidth - (blockWidth * CGFloat(columns)) / 2.0 + offsetWidth
        let offsetHeight = blockHeight /  2.0 + 0.5
        let y = CGFloat(rows - row - 1) * blockHeight - (blockHeight * CGFloat(rows)) / 2.0 + offsetHeight
        return CGPoint(x:x, y:y)
    }
}
