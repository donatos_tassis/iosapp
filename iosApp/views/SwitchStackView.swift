//
//  SwitchStackView.swift
//  iosApp
//
//  Created by user153865 on 4/25/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class SwitchStackView : UIStackView {
    
    var scene : LevelScene!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, scene: LevelScene) {
        self.init()
        self.scene = scene
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // get switch coordinates
        for view in arrangedSubviews {
            let switchRect = view.frame
            let midX = switchRect.midX + (scene.screenWidth - scene.switchWidth)/2
            
            if let component = scene.components as? [String? : Component] {
                let component = component[view.accessibilityIdentifier] as! Component
                scene.componentsCoord[component.getImageViewTag()] = [
                    midX,
                    switchRect.maxY + 25
                ]
            }
        }
    }
}
