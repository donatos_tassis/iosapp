//
//  ProtocolGate.swift
//  iosApp
//
//  Created by user153865 on 4/17/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation

protocol Gate : Component {
    
    // first input of gate
    var inputA:Component { get set }
    
    // second input of gate
    var inputB:Component { get set }
    
    // the row number into the TableLayout that the Gate is going to be placed
    var gridRow:Int { get set }
    
    // the position into the TableRow that the Gate is going to be placed
    var gridColumn:Int { get set }
    
    // the picture of the gate
    var gatePictureId:Int { get set }
    
    // ImageView tag
    var imageViewTag:String { get set }
    
}

