//
//  NotGate.swift
//  iosApp
//
//  Created by user153865 on 4/17/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation


class NotGate : Component {
    
    // component prefix
    let COMPONENT_PREFIX = "not"
    
    // input of the Cpu Component
    internal var input:Component?
    
    // imageView tag
    internal var imageViewTag:String?
    
    // the picture of the gate
    internal var gatePicture:String?
    
    // the row number into the TableLayout that the Gate is going to be placed
    internal var gridRow:Int
    
    // the position into the TableRow that the Gate is going to be placed
    internal var gridColumn:Int
    
    // constructor of Cpu
    init(input:Component, gridRow:Int, gridColumn:Int) {
        self.input = input
        self.gridRow = gridRow
        self.gridColumn = gridColumn
        self.gatePicture = "not_trans.jpg"
        self.imageViewTag = getImageViewTag()
    }
    
    func getType() -> String {
        return "NotGate"
    }
    
    func output() -> Bool {
        return !(getInput().output())
    }
    
    func getPrefix() -> String {
        return COMPONENT_PREFIX
    }
    
    func getGatePicture() -> String {
        return gatePicture!
    }
    
    func getImageViewTag() -> String {
        var imageView = COMPONENT_PREFIX + "_" + String(getGridRow()) + "-" + String(getGridColumn()) + "_"
        
        if(input is Switch) {
            imageView += input!.getPrefix() + "-" + String(input!.getGridColumn())
        } else {
            imageView += input!.getPrefix() + "-" + String(input!.getGridRow()) + "-" + String(input!.getGridColumn())
        }
        
        return imageView
    }
    
    func getGridRow() -> Int {
        return gridRow
    }
    
    func getGridColumn() -> Int {
        return gridColumn
    }
    
    // gets the input Component of the NotGate
    func getInput() -> Component {
        return input!;
    }
    
    // sets the input Component of the NotGate
    func setInput(input:Component) {
        self.input = input;
    }
    
}
