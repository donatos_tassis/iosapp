//
//  NorGate.swift
//  iosApp
//
//  Created by user153865 on 4/17/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation


class NorGate : Gate {
    
    // component prefix
    let COMPONENT_PREFIX = "nor"
    
    
    override init(gridRow:Int, gridColumn:Int) {
        super.init(gridRow: gridRow, gridColumn: gridColumn)
    }
    
    convenience init(gridRow:Int, gridColumn:Int, inputA:Component, inputB:Component) {
        self.init(gridRow: gridRow, gridColumn: gridColumn)
        self.inputA = inputA
        self.inputB = inputB
        self.gatePicture = "nor_trans.jpg"
        self.imageViewTag = getImageViewTag()
    }
    
    override func output() -> Bool {
        return !(self.inputA!.output() || self.inputB!.output())
    }
    
    override func getPrefix() -> String {
        return COMPONENT_PREFIX
    }
    
    override func getGatePicture() -> String {
        return gatePicture!
    }
    
    override func getImageViewTag() -> String {
        var imageView = COMPONENT_PREFIX + "_" + String(getGridRow()) + "-" + String(getGridColumn()) + "_"
        
        if(inputA is Switch) {
            imageView += inputA!.getPrefix() + "-" + String(inputA!.getGridColumn()) + "-"
        } else {
            imageView += inputA!.getPrefix() + "-" + String(inputA!.getGridRow()) + "-" + String(inputA!.getGridColumn()) + "_"
        }
        
        if(inputB is Switch) {
            imageView += inputB!.getPrefix() + "-" + String(inputB!.getGridColumn())
        } else {
            imageView += inputB!.getPrefix() + "-" + String(inputB!.getGridRow()) + "-" + String(inputB!.getGridColumn())
        }
        
        return imageView
    }
    
    override func getGridRow() -> Int {
        return gridRow
    }
    
    override func getGridColumn() -> Int {
        return gridColumn
    }
}
