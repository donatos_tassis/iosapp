//
//  Gate.swift
//  iosApp
//
//  Created by user153865 on 4/17/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation

class Gate : Component {
    
    // first input of gate
    internal var inputA:Component?
    
    // second input of gate
    internal var inputB:Component?
    
    // the row number into the TableLayout that the Gate is going to be placed
    internal var gridRow:Int
    
    // the position into the TableRow that the Gate is going to be placed
    internal var gridColumn:Int
    
    // the picture of the gate
    internal var gatePicture:String?
    
    // ImageView tag
    internal var imageViewTag:String?
    
    
    init(gridRow:Int, gridColumn:Int) {
        self.inputA = nil
        self.inputB = nil
        self.imageViewTag = nil
        self.gatePicture = nil
        self.gridRow = gridRow
        self.gridColumn = gridColumn
    }
    
    func getType() -> String {
        return "Gate"
    }
    
    func output() -> Bool {
        preconditionFailure("Method 'output' must be overridden")
    }
    
    func getPrefix() -> String {
        preconditionFailure("Method 'getPrefix' must be overridden")
    }
    
    func getGatePicture() -> String {
        preconditionFailure("Method 'getGatePicture' must be overridden")
    }
    
    func getImageViewTag() -> String {
        preconditionFailure("Method 'getImageViewTag' must be overridden")
    }
    
    func getGridRow() -> Int {
        preconditionFailure("Method 'getGridRow' must be overridden")
    }
    
    func getGridColumn() -> Int {
        preconditionFailure("Method 'getGridColumn' must be overridden")
    }
}
