//
//  Component.swift
//  iosApp
//
//  Created by user153865 on 4/16/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation

protocol Component : class {
    
    // output of the Component depending on the input(s)
    func output() -> Bool
    
    // returns a string that represents he classes prefix
    func getPrefix() -> String
    
    // gets the gates picture path name
    func getGatePicture() -> String
    
    // gets the Components ImageView tag
    func getImageViewTag() -> String
    
    // gets the type of the Component
    func getType() -> String
    
    /* Not sure if its gonna be used */
    // gets the gridRow index that the Component belongs
    func getGridRow() -> Int
    
    /* Not sure if its gonna be used */
    // gets the gridRow's position that the Component belongs
    func getGridColumn() -> Int
}
