//
//  Switch.swift
//  iosApp
//
//  Created by user153865 on 4/16/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation

public class Switch : Component {
    
    // component prefix
    let COMPONENT_PREFIX = "swi"
    
    // imageView tag
    internal var imageViewTag:String
    
    // the status of the switch on/off true/false respectively
    var status:Bool = false
    
    // the position into the LinearLayout that the Switch is going to be placed
    internal var gridColumn:Int
    
    // constructor of Switch
    init(gridColumn:Int) {
        self.gridColumn = gridColumn
        self.imageViewTag = COMPONENT_PREFIX + "-" + String(gridColumn)
    }
    
    func getType() -> String {
        return "Switch"
    }
    
    func output() -> Bool {
        return getStatus()
    }
    
    func getPrefix() -> String {
        return COMPONENT_PREFIX
    }
    
    func getGatePicture() -> String {
        return ""
    }
    
    func getImageViewTag() -> String {
        return self.imageViewTag
    }
    
    func getGridRow() -> Int {
        return -1
    }
    
    func getGridColumn() -> Int {
        return self.gridColumn
    }
    
    // change the status of the Switch from on to off and visa versa
    func chanceStatus() {
        status = getStatus()
        self.status = !status
    }
    
    // gets the status of the Switch
    func getStatus() -> Bool {
        return self.status
    }
    
}
