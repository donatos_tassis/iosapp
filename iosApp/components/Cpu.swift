//
//  Cpu.swift
//  iosApp
//
//  Created by user153865 on 4/16/19.
//  Copyright © 2019 stc010. All rights reserved.
//

import Foundation

public class Cpu : Component {
    
    // component prefix
    let COMPONENT_PREFIX = "cpu"
    
    // input of the Cpu Component
    internal var input:Component
    
    // imageView tag
    internal var imageViewTag:String
    
    // constructor of Cpu
    init(input:Component) {
        self.input = input
        var imageView = COMPONENT_PREFIX + "_" + input.getPrefix() + "-"
        imageView += String(input.getGridRow()) + "-" + String(input.getGridColumn())
        self.imageViewTag = imageView
    }
    
    func getType() -> String {
        return "Cpu"
    }
    
    func output() -> Bool {
        return getInput().output()
    }
    
    func getPrefix() -> String {
        return COMPONENT_PREFIX
    }
    
    func getGatePicture() -> String {
        return ""
    }
    
    func getImageViewTag() -> String {
        return imageViewTag
    }
    
    func getGridRow() -> Int {
        return -1
    }
    
    func getGridColumn() -> Int {
        return -1
    }
    
    // gets the input Component of the Cpu
    func getInput() -> Component {
        return input;
    }
    
    // sets the input Component of the Cpu
    func setInput(input:Component) {
        self.input = input;
    }
    
}
